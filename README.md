# dwm
My custom configured build of suckless's dynamic window manager for X

# Contents
<!-- vim-markdown-toc GFM -->

* [Features](#features)
    * [Patches](#patches)
    * [Other](#other)
* [Install](#install)

<!-- vim-markdown-toc -->

# Features
## Patches
See this repo's `patches` directory for the most up to date list of patches
I've applied.

Also check out [dwm.suckless.org](https://dwm.suckless.org) to understand what
these patches do.

## Other
- XF86 keybindings for AudioMute, AudioRaiseVolume, AudioLowerVolume,
  MonBrightnessUp and MonBrightnessDown
    - Uses `amixer` and `brightnessctl`
- Customized keybindings and colors
- replaces `config.h` with `config.def.h` every time `make clean` is ran
- `dwm.desktop` file for those using xsession and display manager

# Install
```bash
git clone https://gitlab.com/sawfishswordstaff/dwm
cd dwm
make clean install # requires ROOT privileges by default
```

